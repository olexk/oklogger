
#include "oklogger/oklogger.h"
#include <stdio.h>
#include <stdarg.h>

static bool bLoggingInitialized = false;

void InitializeLogging(const char *logDirPath, const char *logFileName)
{
    if (!bLoggingInitialized)
    {
        bLoggingInitialized = true;
    }
}

bool IsDebugLoggingEnabled()
{
    return false;
}

void LogMessageFmt(const char *fmt, ...)
{
    char buf[16048];
    va_list ap;

    va_start(ap, fmt);
    sprintf(buf, fmt, ap);
    va_end(ap);

    printf("%s\r\n", buf);
}