#ifndef NJ_LOGGER_H_
#define NJ_LOGGER_H_

extern void InitializeLogging(const char* logDirPath, const char * logFileName);
extern bool IsDebugLoggingEnabled();
extern void LogMessageFmt(const char *fmt, ...);

#define LOG_INFO(msg) LogMessage(msg)

#define LOG_DEBUG(fmt, ...)                          \
    {                                                \
        if (IsDebugLoggingEnabled())                 \
        {                                            \
            LogMessage(fmt, __VA_ARGS__); \
        }                                            \
    }

#endif